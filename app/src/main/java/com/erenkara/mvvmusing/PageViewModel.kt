package com.erenkara.mvvmusing

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class PageViewModel : ViewModel() {

    var mathRepository = MathRepository()
    var sonuc = MutableLiveData<String>()

    init {
        sonuc = mathRepository.mathResultGet()
    }

    fun toplamaYap(number1: String, number2: String) {
        mathRepository.topla(number1, number2)
    }

    fun carpmaYap(number1: String, number2: String) {
        mathRepository.carp(number1, number2)
    }
}