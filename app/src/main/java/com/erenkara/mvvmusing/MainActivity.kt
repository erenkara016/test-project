package com.erenkara.mvvmusing

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.erenkara.mvvmusing.ui.theme.MvvmUsingTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MvvmUsingTheme {
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colors.background) {
                    Page()
                }
            }
        }
    }
}

@Composable
fun Page() {

    val viewModel: PageViewModel = viewModel()

    val tfNumber1 = remember { mutableStateOf("") }
    val tfNumber2 = remember { mutableStateOf("") }
    val result = viewModel.sonuc.observeAsState("0")


    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.SpaceEvenly,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(text = result.value, fontSize = 50.sp)
        TextField(
            value = tfNumber1.value,
            onValueChange = { tfNumber1.value = it },
            label = {
                Text("Sayı 1")
            }
        )

        TextField(
            value = tfNumber2.value,
            onValueChange = { tfNumber2.value = it },
            label = {
                Text("Sayı 2")
            }
        )

        Button(onClick = {
            viewModel.toplamaYap(tfNumber1.value, tfNumber2.value)

        }) {
            Text("Topla")
        }

        Button(onClick = {
            viewModel.carpmaYap(tfNumber1.value, tfNumber2.value)

        }) {
            Text("Çarp")
        }
    }
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    MvvmUsingTheme {
        Page()
    }
}