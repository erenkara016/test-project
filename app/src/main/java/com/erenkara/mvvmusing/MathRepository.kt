package com.erenkara.mvvmusing

import androidx.lifecycle.MutableLiveData

class MathRepository {
    var mathResult = MutableLiveData<String>()

    init {
        mathResult = MutableLiveData<String>("0")
    }

    fun mathResultGet() : MutableLiveData<String>{
        return mathResult
    }

    fun topla(number1: String, number2: String) {
        val sayi1 = number1.toInt()
        val sayi2 = number2.toInt()

        val toplam = sayi1 + sayi2

        mathResult.value = toplam.toString()
    }

    fun carp(number1: String, number2: String) {
        val sayi1 = number1.toInt()
        val sayi2 = number2.toInt()

        val carpma = sayi1 * sayi2

        mathResult.value = carpma.toString()
    }
}